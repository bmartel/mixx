const path = require('path')
const webpack = require('webpack')
const mixx = require('./webpack')
const pkg = require('./package.json')

module.exports = {
  devtool: (process.env.NODE_ENV === 'production') ? '' : 'sourcemap',
  context: path.join(__dirname, 'src'),
  entry: { 
    mixx: ['./mixx.css'],
    index: ['./styleguide.html']
  },
  output: {
    path: path.resolve(pkg.config.buildDir),
    publicPath: '/',
    filename: '[name].js',
  },
  module: {
    loaders: mixx.loaders,
  },
  plugins : [
    mixx.cssExtractTextPlugin,
    mixx.cssCompressionPlugin,
  ]
}