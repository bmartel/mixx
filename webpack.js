const ExtractTextPlugin = require('extract-text-webpack-plugin')
const CompressionPlugin = require("compression-webpack-plugin")

const cssLoaders = [
  {
    test: /\.css$/,
    loader: ExtractTextPlugin.extract({ fallback: 'style-loader', use: ['css-loader', 'postcss-loader'] }),
  },
]

const fontLoaders = [
  {
    test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
    loader: 'url-loader?limit=5000&mimetype=application/font-woff',
  },
  {
    test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
    loader: 'url-loader?limit=5000&mimetype=application/octet-stream',
  },
  {
    test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
    loader: 'url-loader?limit=5000&mimetype=application/vnd.ms-fontobject',
  },
]

const svgLoaders = [
  {
    test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
    use: [
      'svg-sprite-loader',
      'svg-fill-loader',
      { loader: 'svgo-loader', options: { removeTitle: true } },
    ],
  },
]

const imageLoaders = [
  {
    test: /\.(jpe?g|png|gif)$/i,
    loaders: [
      'file-loader?hash=sha512&digest=hex&name=[path][hash].[ext]', {
        loader: 'image-webpack-loader',
        query: {
          mozjpeg: {
            progressive: true,
          },
          gifsicle: {
            interlaced: false,
          },
          optipng: {
            optimizationLevel: 7,
          },
          pngquant: {
            quality: '65-90',
            speed: 4,
          },
        },
      },
    ],
  }
]

const htmlLoaders = [
  {
    test: /\.html$/,
    loader: 'file-loader?name=[path]index.[ext]',
  },
]

const cssExtractTextPlugin = new ExtractTextPlugin({
  filename: '[name].css',
  publicPath: '/',
  allChunks: true,
})

const cssCompressionPlugin = new CompressionPlugin({
  asset: "[path].gz[query]",
  algorithm: "gzip",
  test: /\.(css)$/,
  threshold: 10240,
  minRatio: 0.8
})

const loaders = [
  ...fontLoaders,
  ...svgLoaders,
  ...cssLoaders,
  ...htmlLoaders,
]

const plugins = [
  require('postcss-import'),
  require('postcss-sassy-mixins'),
  require('postcss-extend'),
  require('postcss-functions'),
  require('postcss-cssnext'),
  require('rucksack-css'),
  require('lost'),
]

module.exports = {
  cssCompressionPlugin,
  cssExtractTextPlugin,
  fontLoaders,
  svgLoaders,
  cssLoaders,
  htmlLoaders,
  loaders,
  plugins,
}